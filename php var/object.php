<?php
//Declare a simple function to return an
// arrey from our object
function get_students($obj)
{
    if (!is_object($obj)) {
        return false;
    }

    return $obj->students;
}

//Declare anew class instance and fill up
//some values

$obj = new stdClass();
$obj->students = array('Taher', 'Arif', 'Sakil');

var_dump(get_students(null));
var_dump(get_students($obj));
?>