<?php
$var = '';
//This will evaluate to true so the text will be printed
if (isset($var)) {
    echo "This var is set so I will print.";

}

//In teh next fexamples we'll use var_dump to out put
//the return value of isset().

$a ="test";
$b = "anothertest";

var_dump(isset($a));
var_dump(isset($a, $b));

unset ($a);

var_dump(isset($a));
var_dump(isset($a, $b));

$foo = NULL;
var_dump(isset($foo));
?>